import UIKit


let screenHeight = UIScreen.main.bounds.height
let screenWidth = UIScreen.main.bounds.width
let appDelegate = UIApplication.shared.delegate as! AppDelegate


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   
   var window: UIWindow?
   
   
   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
      window = UIWindow(frame: UIScreen.main.bounds)
      window!.rootViewController = ViewController()
      window!.makeKeyAndVisible()
      
      return true
   }
   
   
   
}

