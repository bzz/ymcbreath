import UIKit
import AVFoundation
import CoreAudio


class ViewController: UIViewController {
   
   
   var textField = UILabel()
   var angle : CGFloat = 0
   
   
   var recorder: AVAudioRecorder!
   var levelTimer = Timer()
   
   
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      view.backgroundColor = UIColor.white
      
      textField = UILabel(frame: CGRect(x:50, y:screenHeight/2, width:screenWidth-100, height:100))
      textField.transform = CGAffineTransform(rotationAngle: 1)
      textField.backgroundColor = UIColor.red
      textField.text = "blow"
      textField.textColor = UIColor.white
      view.addSubview(textField)
      

      //make an AudioSession, set it to PlayAndRecord and make it active
      
      let audioSession:AVAudioSession = AVAudioSession.sharedInstance()

      do {
         try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
         try audioSession.setActive(true)
      } catch {
         print(error)
      }


      //set up the URL for the audio file
      let documents: AnyObject = NSSearchPathForDirectoriesInDomains( FileManager.SearchPathDirectory.documentDirectory,  FileManager.SearchPathDomainMask.userDomainMask, true)[0] as AnyObject
      let str =  (documents as! String) + "/recordTest.caf"
      let url = NSURL.fileURL(withPath: str as String)

      
      
      // make a dictionary to hold the recording settings so we can instantiate our AVAudioRecorder
      let recordSettings: [String : Any] = [AVFormatIDKey:kAudioFormatAppleIMA4,
                                                    AVSampleRateKey:44100.0,
                                                    AVNumberOfChannelsKey:2,AVEncoderBitRateKey:12800,
                                                    AVLinearPCMBitDepthKey:16,
                                                    AVEncoderAudioQualityKey:AVAudioQuality.max.rawValue]
      
      
      
      
      do {
         try recorder = AVAudioRecorder(url: url, settings: recordSettings )
         recorder.prepareToRecord()
         recorder.isMeteringEnabled = true
         
         //start recording
         recorder.record()
         
         //instantiate a timer to be called with whatever frequency we want to grab metering values
         self.levelTimer = Timer.scheduledTimer(timeInterval: 0.02, target: self, selector: #selector(ViewController.levelTimerCallback), userInfo: nil, repeats: true)
         

      } catch {
         print(error)
      }

      
   }
   

   
   //This selector/function is called every time our timer (levelTime) fires
   func levelTimerCallback() {
      //we have to update meters before we can get the metering values
      recorder.updateMeters()
      
      //print to the console if we are beyond a threshold value. Here I've used -7
      let a = recorder.averagePower(forChannel: 0)
      if a > -40 {
         print(a)
         angle += (CGFloat(a)+40)/50
         textField.transform = CGAffineTransform(rotationAngle: angle)
      }
   }
}


